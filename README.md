# CCA2 Encryption Project
## Usage
```
Usage: ./kem-enc [OPTIONS]...
Encrypt or decrypt data.

   -i,--in     FILE   read input from FILE.
   -o,--out    FILE   write output to FILE.
   -k,--key    FILE   the key.
   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).
   -e,--enc           encrypt (this is the default action).
   -d,--dec           decrypt.
   -g,--gen    FILE   generate new key and write to FILE{,.pub}
   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the
                      RSA key; the symmetric key will always be 256 bits).
                      Defaults to 1024.
   --help             show this message and exit.
```

## Notes
When generating a key, specify the filename **without** extension. When generating a key, two files are created: ```<keyname>.pub``` and ```<keyname>.prv``` which are the public key and private key respectively. Example:
`./kem-enc -e -k testKey -i infile.txt -o outfile.txt`
This modification was done in order to be able to send the keyfile to other users and still keep the secret key.
