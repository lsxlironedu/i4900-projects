#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* buf = malloc(len);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(buf,len,x);
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	int keyBytes = keyBits/8;
	rsa_initKey(K);
	unsigned char *buffer = malloc(keyBits);
	
	
	mpz_t p, q, n, e, d;
	mpz_t phi_n, phi_q, phi_p, gcd;
	
	mpz_init(p);
	mpz_init(q);
	mpz_init(n);
	mpz_init(e);
	mpz_init(d);
	mpz_init(gcd);
	mpz_init(phi_q);
	mpz_init(phi_n);
	mpz_init(phi_p);

	
	//Choose prime random p
	randBytes(buffer, keyBytes);
	BYTES2Z(p, buffer, keyBytes);
	while (ISPRIME(p) == 0)
	{
		mpz_nextprime(p,p);
	}
	
	//Choose prime random q
	randBytes(buffer, keyBytes);
	BYTES2Z(q, buffer, keyBytes);
	while (ISPRIME(q) == 0)
	{
		mpz_nextprime(q,q);
	}

	// Get n
	mpz_mul(n, p, q);


	//Get phi_n
	mpz_sub_ui(phi_q, q, 1);
	mpz_sub_ui(phi_p, p, 1);
	mpz_mul(phi_n, phi_p, phi_q);
	
	// Get e such that GCD(phi_n,e)=1
	mpz_set_ui(gcd, 0);
	while(mpz_cmp_ui(gcd, 1) != 0)
	{
		randBytes(buffer, 2*keyBytes);
		BYTES2Z(e, buffer, 2*keyBytes);
		mpz_gcd(gcd, e, phi_n);
	}

	// Find d = (e^-1) mod n
	mpz_invert(d, e, phi_n);


	// RSA key setup
	mpz_set(K->p, p);
	mpz_set(K->q, q);
	mpz_set(K->e, e);
	mpz_set(K->n, n);
	mpz_set(K->d, d);

	
	/* TODO: write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */

	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	
	mpz_t msg, cipher;
	mpz_init(msg);
	mpz_init(cipher);

	
	//Get the message, encrypt and convert back to bytes
	BYTES2Z(msg, inBuf, len);
	mpz_powm(cipher, msg, K->e, K->n);
	Z2BYTES(outBuf, len, cipher);

	return len; 
}

size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	mpz_t msg, cipher;
	mpz_init(msg);
	mpz_init(cipher);
	
	// Get the cipher, decrypt and convert back to bytes
	BYTES2Z(cipher, inBuf, len);
	mpz_powm(msg, cipher, K->d, K->n);
	Z2BYTES(outBuf, len, msg);
	return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}