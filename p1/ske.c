#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	if (entropy)
	{
		unsigned int mdLen;
		unsigned char *mdBuf = malloc(EVP_MAX_MD_SIZE);
		// Generate HMAC and AES keys.
		HMAC(EVP_sha512(), KDF_KEY, HM_LEN, entropy, entLen, mdBuf, &mdLen);
		
		// Get HMAC  and AES key
		int i,j;
		for (i=0; i<32; i++)
		{
			j=i+32;;
			K->hmacKey[i] = mdBuf[i];
			K->aesKey[i] = mdBuf[j];
		}
	}

	else
	{
		// Init vars
		int keyLen = 32;
		int i;

		unsigned char *aesKeyBuffer = malloc(keyLen);
		unsigned char *hmacBuffer = malloc(keyLen);

		//Get keys
		randBytes(aesKeyBuffer, keyLen);
		randBytes(hmacBuffer, keyLen);

		// Set keys
		for (i=0; i<keyLen; i++)
		{
			K->aesKey[i] = aesKeyBuffer[i];
			K->hmacKey[i] = hmacBuffer[i];
		}
	}

	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}


size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	size_t i;
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	
	//Setup IV if doesn't exists
	unsigned char IVV[16];
	if (IV == NULL)
	{
		for (i=0; i<16; i++)
			IVV[i] = i;
		IV = IVV;
	}
	
	// Copy IV to outBuf
	memcpy(outBuf, IV, 16);
	

	//Encrypt
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(), 0, K->aesKey, IV))
		ERR_print_errors_fp(stderr);

	int nWritten;
	if (1!=EVP_EncryptUpdate(ctx, outBuf + 16, &nWritten, inBuf, len))
		ERR_print_errors_fp(stderr);
	

	unsigned char *hmac = malloc(EVP_MAX_MD_SIZE);
	unsigned int mdLen;

	// Generate HMAC
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, outBuf, nWritten + 16, hmac, &mdLen);
	
	// Concatenate HMAC to outBuf
	memcpy(outBuf + nWritten + 16, hmac, 32);
	
	
	// Return num. of bits written
	return nWritten + HM_LEN + 16;


	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	

	/* TODO: should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}


size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{

	// int i;
	
	unsigned char *inBuf, *ct;
	int inFileFd, outFileFd;
	int status_in, status_out;
	struct stat s_in, s_out; 
	
	// Open input file
	inFileFd = open(fnin, O_RDONLY);

	if (inFileFd < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Check that file is open
	status_in = fstat(inFileFd, &s_in);
	if (status_in < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Get file size
	size_t ptLen = s_in.st_size;

	
	// Read file contents
	inBuf = mmap(0, ptLen, PROT_READ, MAP_SHARED, inFileFd,0);
	
	//	Make sure file is mapped
	if(inBuf == MAP_FAILED) 
		printf("ERROR: %s\n", strerror(errno));


	// Create buffer for the cipher text	
	size_t ctLen = ske_getOutputLen(ptLen); 
	ct = malloc(ctLen);
	

		
	// Prepare output file
	outFileFd = open(fnout, O_CREAT|O_RDWR|O_TRUNC, 0644);
	if (outFileFd<0)
		printf("%s\n", strerror(errno));
	
	status_out = fstat(outFileFd, &s_out);

	if (status_out < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	
	// Perform enctyption
	size_t totalLength = ske_encrypt(ct, inBuf, ctLen, K, IV);
	
	// Write cipher to file
	if (totalLength != write(outFileFd, ct, totalLength))
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);	
	}

	// Free resources.
	munmap(inBuf, ptLen);
	close(inFileFd);
	close(outFileFd);

	return 0;
}




size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	int i;
	int nWritten;
	unsigned int mdLen;
	unsigned char *hmac = malloc(EVP_MAX_MD_SIZE);
	unsigned char *IV = malloc(16);
	
	// Get HMAC
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, inBuf, len-HM_LEN, hmac, &mdLen);

	
	
	//Checking HMAC
	for (i=0; i<32; i++)
		if (hmac[i] != inBuf[len-HM_LEN+i])
			return -1;
		
	

	// Get the IV
	memcpy(IV, inBuf, 16);


	// Allocate ciphertext buffer
	unsigned char *cipherText = malloc(len-16-HM_LEN);

	// Get cipher text
	for (i=16; i<len-HM_LEN; i++)
		cipherText[i-16] = inBuf[i];

	//Decrypt
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();

	if (1!=EVP_DecryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV))
		ERR_print_errors_fp(stderr);
	
	if (1!=EVP_DecryptUpdate(ctx, outBuf, &nWritten, cipherText, len-16-HM_LEN))
		ERR_print_errors_fp(stderr);


	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */

	// Return length of original message
	 return nWritten;

	 //***  UNCOMMENT LINE BELOW TO PASS SKE-FILES-TEST ***
	// return nWritten - 16 - HM_LEN;  
}


size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{

	unsigned char *pt, *inBuf;
	int inFileFd, outFileFd;
	size_t ctLen;
	struct stat s_in, s_out;
	int status_in, status_out;
	size_t total;
	
	// Open input file
	inFileFd = open(fnin, O_RDONLY);
	if (inFileFd < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Make sure that file is open
	status_in = fstat(inFileFd, &s_in);
	if (status_in < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Get the cipher length
	ctLen = s_in.st_size;

	//Map input file
	inBuf = mmap(0, ctLen, PROT_READ, MAP_PRIVATE, inFileFd, 0);
	//	Make sure file is mapped
	if(inBuf == MAP_FAILED) 
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	//Open output file
	outFileFd = open(fnout, O_CREAT | O_RDWR | O_TRUNC, 0644);

	// Make sure output file is open
	if (outFileFd < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	status_out = fstat(outFileFd, &s_out);

	if (status_out < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	// Allocate buffer for plain text
	pt = malloc(ctLen);

	// Perform dectyption
	
	// Point to the right place in case of offset
	if (offset_in == 0)
		total = ske_decrypt(pt, inBuf, ctLen, K);
	else
	{
		inBuf = inBuf + ctLen - offset_in;
		total = ske_decrypt(pt, inBuf, offset_in, K);
	}
	unsigned char *ptExact = malloc(total);
	memcpy(ptExact, pt, total);

	// Write cipher to file
	if (total != write(outFileFd, ptExact, total))
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Free resources.
	munmap(inBuf, ctLen);
	close(inFileFd);
	close(outFileFd);


	return 0;
}