/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <errno.h>
#include <unistd.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>


#include "ske.h"
#include "rsa.h"
#include "prf.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
#define HM_LEN 32

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{

	//Setup random message
	size_t keyLen = rsa_numBytesN(K);
	unsigned char *ptKey = malloc(keyLen);
	unsigned char *ctKey = malloc(keyLen);

	//Encrypt random message using RSA
	randBytes(ptKey, keyLen-1);
	rsa_encrypt(ctKey, ptKey, keyLen, K);
	
	
	//Create HMAC for the random message
	unsigned int mdLen;
	unsigned char *mdBuf = malloc(EVP_MAX_MD_SIZE);
	HMAC(EVP_sha512(), KDF_KEY, HM_LEN, ptKey, keyLen, mdBuf, &mdLen);
	

	// Use SKE to encrypt message
	int inFileFd, outFileFd;
	struct stat s_in, s_out;
	int status_in, status_out;
	int ptLen, ctLen;
	unsigned char *inBuf, *outBuf, *finalBuf;

	SKE_KEY SK;
	ske_keyGen(&SK, mdBuf, mdLen);


	// Open input file
	inFileFd = open(fnIn, O_RDONLY);
	if (inFileFd < 0)
	{
		printf ("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	status_in = fstat(inFileFd, &s_in);
	if (status_in < 0)
	{
		printf ("%s\n", strerror(errno));
		exit(EXIT_FAILURE);	
	}

	//Get plain text length
	ptLen = s_in.st_size;

	// Get ciphertext length
	ctLen = ske_getOutputLen(ptLen); 


	// Map input file
	inBuf = mmap(0, ptLen, PROT_READ, MAP_SHARED, inFileFd, 0);

	// Allocate cipher text buffer
	outBuf = malloc(ctLen);

	// Encrypt
	int total = ske_encrypt(outBuf, inBuf, ptLen, &SK, NULL);
	int finalLength = total+keyLen+mdLen;

	// Create encapsulation
	finalBuf = malloc(finalLength);
	memcpy(finalBuf, ctKey, keyLen); // Copy rsa cipher
	memcpy(finalBuf + keyLen, mdBuf, mdLen); //Copy hmac
	memcpy(finalBuf + keyLen + mdLen, outBuf, total); // Copy SKE cipher

	/* Structure:
	* +------------+--------------------------+------------+
	* | RSA cipher | RSA plaintext HMAC  (64) | SKE cipher |
	* +------------+--------------------------+------------+
	*/



	

	// Write to file
	// -------------

	// Open output file
	outFileFd = open(fnOut, O_CREAT|O_RDWR|O_TRUNC, 0644);
	if (outFileFd < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	// Make sure file is open
	status_out = fstat(outFileFd, &s_out);
	if (status_out < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Write output to file
	if (finalLength != write(outFileFd, finalBuf, finalLength))
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Free resources
	munmap(inBuf, ptLen);
	close(inFileFd);
	close(outFileFd);

	
	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */
	return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */
	int inFileFd;
	struct stat s_in;
	int status_in;
	unsigned char *inBuf;
	unsigned char *rsaCt, *rsaPt;
	size_t ctLen, keyLen;
	int mdLen;
	unsigned char *origHmac;
	unsigned char *mdBuf;
	unsigned	char *skeCt;
	int i;
	
	// Read inFile
	inFileFd = open(fnIn, O_RDONLY);

	if (inFileFd < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	
	// Make sure file is open
	status_in = fstat(inFileFd, &s_in);
	if (status_in < 0)
	{
		printf("%s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	// Get cipertext size
	ctLen = s_in.st_size;

	// Map file as buffer
	inBuf = mmap(0, ctLen, PROT_READ, MAP_SHARED, inFileFd, 0);

	//Get random bytes for the key
	keyLen = rsa_numBytesN(K);

	
	// Copy encrypted key
	rsaCt = malloc(keyLen);
	memcpy(rsaCt, inBuf, keyLen); // RSA cipher

	origHmac = malloc(64);
	memcpy(origHmac, inBuf + keyLen, 64); // RSA HMAC

	skeCt = malloc(ctLen-64-keyLen);
	memcpy(skeCt, inBuf + keyLen + 64,  ctLen-64-keyLen); // SKE cipher

	//Decrypt RSA
	rsaPt = malloc(keyLen);
	rsa_decrypt(rsaPt, rsaCt, keyLen, K);

	// Create HMAC from RSA plaintext
	mdBuf = malloc(EVP_MAX_MD_SIZE);
	HMAC(EVP_sha512(), KDF_KEY, HM_LEN, rsaPt, keyLen, mdBuf, &mdLen);

	
	// Verify that HMAC are matching
	for (i=0; i<64; i++)
	{
		if (origHmac[i] != mdBuf[i])
		{
			printf("Invalid ciphertext...");
			return -1;
		}
	}
	
	// Generate SKE key
	SKE_KEY SK;
	ske_keyGen(&SK, mdBuf, (size_t)mdLen);
	
	// Close input buffer
	close(inFileFd);
	size_t offset = ctLen-64-keyLen;
	ske_decrypt_file(fnOut, fnIn, &SK, offset);

	return 0;
}

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	// size_t nBits = 2048;
	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */

	RSA_KEY K;
	FILE *pubFile;
	FILE *prvFile;
	char pubName[100] = "";
	char prvName[100] = "";
	
	switch (mode) {
		case ENC:
		
			// Generate public key filename
			strcpy(pubName, fnKey);
			strcat(pubName, ".pub");
			pubFile = fopen(pubName, "r");
			
			// Read key from file
			rsa_readPublic(pubFile, &K);
			fclose(pubFile);
			
			// Encrypt
			kem_encrypt(fnOut, fnIn, &K);
			break;

		case DEC:
			
			//Get private key file
			strcpy(prvName, fnKey);
			strcat(prvName, ".prv");

			// Open file
			prvFile = fopen(prvName, "r");
			

			// Read private key
			rsa_readPrivate(prvFile, &K);			
			
			// printf("DONE READING\n");

			// Close files
			fclose(prvFile);


			// Decrypt
			kem_decrypt(fnOut, fnIn, &K);

			// Destroy keys
			rsa_shredKey(&K);
			break;

		case GEN:
			//Create file names
			strcpy(pubName, fnOut);
			strcat(pubName, ".pub");

			strcpy(prvName, fnOut);
			strcat(prvName, ".prv");

			//Generate key
			rsa_keyGen(nBits, &K);

			pubFile = fopen(pubName, "w+");
			rsa_writePublic(pubFile, &K);
			fclose(pubFile);


			prvFile = fopen(prvName, "w+");
			rsa_writePrivate(prvFile, &K);
			fclose(prvFile);			
			break;

		default:
			return 1;
	}

	return 0;
}
